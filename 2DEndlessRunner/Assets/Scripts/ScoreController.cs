﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    
    private int _currentScore;
    [Header("Score Highlight")]
    public int _scoreHighlightRange;
    public ScAudioController _sound;
    private int _lastScoreHighlight = 0;
    void Start()
    {
        _currentScore = 0;
        _lastScoreHighlight = 0;
    }

    public float GetCurrentScore()
    {
        return _currentScore;
    }

    public void IncreaseCurrentScore(int increment)
    {
        _currentScore += increment;
        if (_currentScore - _lastScoreHighlight > _scoreHighlightRange ){
            _sound.PlayScoreHighlight();
            _lastScoreHighlight += _scoreHighlightRange;
        }
    }

    public void FinishScoring()
    {
        if (_currentScore > ScScoreData._highScore)
        {
            ScScoreData._highScore = _currentScore;
        }
    }

    public void JumpSound(){
          _sound.PlayJump();
    }

}

    // Update is called once per frame
   

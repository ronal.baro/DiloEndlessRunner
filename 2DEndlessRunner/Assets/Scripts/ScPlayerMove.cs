﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScPlayerMove : MonoBehaviour
{
    // Start is called before the first frame update
    [Header("Movement")]
    public float _maxSpeed;
    public float _moveAccel;

    Rigidbody2D _rbPlayer;
    

    [Header("Jump")]
    public float _jumpAccel;
    public float _fallAnimStart=8f;
    private bool _isJumping;
    private bool _isOnGround;

    private bool _isFalling;

    Animator _AnimPlayer;

    [Header("GroundRaycast")]
    public float _groundRaycastDistance;
    public LayerMask _groundLayerMask;

    [Header("Scoring")]
    public ScoreController _score;
    public float _scoringRatio;

    private float _lastPositionX;

    [Header("Game Over")]
    public GameObject gameOverScreen;
    public float fallPositionY;
    [Header("Camera")]
    public ScCameraMove gameCamera;
    void Start()
    {
        _rbPlayer = GetComponent<Rigidbody2D>();
        //slower katanya _AnimPlayer = GameObject.Find("Badan").GetComponent<Animator>();
        //_AnimPlayer = GetComponentInChildren<Animator>();
        _AnimPlayer = this.transform.Find("Badan").GetComponent<Animator>();
      
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_isOnGround)
            {
                _isJumping = true;
            }
        }

         //Scoring

        int distancePassed = Mathf.FloorToInt(transform.position.x - _lastPositionX);
        int scoreIncrement =  Mathf.FloorToInt(distancePassed / _scoringRatio);
        if (scoreIncrement>0)
        {
            _score.IncreaseCurrentScore(scoreIncrement);
            _lastPositionX += distancePassed;
        }

        //Game Over Checking
        if (transform.position.y <fallPositionY){
            GameOver();
        }

    }
    private void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, _groundRaycastDistance, _groundLayerMask);
        if (hit)
        {
         //   Debug.Log(!_isOnGround + " " + (_rbPlayer.velocity.y).ToString("0.00"));

            if (!_isOnGround && _rbPlayer.velocity.y < 0.05f)
            {
                _isOnGround = true;
               
            }
        }
        else
        {
            _isOnGround = false;
           
         if(!_isOnGround &&_rbPlayer.velocity.y<-_fallAnimStart){
                _isFalling = true;
            }
            else{
                _isFalling = false;
            }
            _AnimPlayer.SetBool("isFalling",_isFalling);
        }

        
         _AnimPlayer.SetBool("isOnGround",_isOnGround);

        

        Vector2 velocity = _rbPlayer.velocity;
        if (_isJumping)
        {
            velocity.y += _jumpAccel;
            _score.JumpSound();
            _isJumping = false;
        }

        velocity.x = Mathf.Clamp(velocity.x + _moveAccel * Time.fixedDeltaTime, 0.0f, _maxSpeed);

        _rbPlayer.velocity = velocity;

       
    }

    void GameOver(){
        _score.FinishScoring();

        gameCamera.enabled = false;
        gameOverScreen.SetActive(true);
        this.enabled = false;
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * _groundRaycastDistance), Color.red);
    }
}

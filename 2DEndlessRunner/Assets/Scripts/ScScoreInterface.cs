﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScScoreInterface : MonoBehaviour
{
    [Header ("UI")]
    public Text Score;
    public Text highScore;
    [Header ("Score")]
    public ScoreController scoreController;
    private void Update() {
        Score.text = scoreController.GetCurrentScore().ToString().PadLeft(9,'0');
        highScore.text = ScScoreData._highScore.ToString("000000000");
    }
    }


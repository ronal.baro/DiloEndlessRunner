﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTerrainTemplateController : MonoBehaviour
{     
    private const float _debugLineHeight = 10.0f;

    private void OnDrawGizmos() {
        Debug.DrawLine(transform.position +Vector3.up * _debugLineHeight/2, transform.position + Vector3.down * _debugLineHeight/2,Color.green);
    }

}

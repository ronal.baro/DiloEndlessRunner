﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScCameraMove : MonoBehaviour
{
    // Start is called before the first frame update

    [Header("Position")]
    public Transform _player;
    public float _horizontalOffset;
    

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = transform.position;
        newPosition.x = _player.position.x + _horizontalOffset;

        transform.position = newPosition;
    }
}

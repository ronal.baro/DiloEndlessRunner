﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTerrainGeneratorController : MonoBehaviour
{
    [Header("Templates")]
     public List<ScTerrainTemplateController> _terrainFirst;
        public List<ScTerrainTemplateController> _terrainTemplateList;
    public float _terrainTemplateWidth=1;

    [Header("Generator Area")]
    public Camera _camera;
    public float _areaStartOffset;
    public float _areaEndOffset;

    public const float _debugLineHeight = 10.0f;

    //Generator
    private List<GameObject> _spawnedTerrain;

    private float _lastGeneratedPosX;

    private float _lastRemovedPosX;

    private void Start() {
        _spawnedTerrain = new List<GameObject>();

        _lastGeneratedPosX = GetHorizontalPositionStart();
        //Horizontal posisi start && Posisi End selalu sejajar dengan karakter
        _lastRemovedPosX = _lastGeneratedPosX - _terrainTemplateWidth;
        
        while (_lastGeneratedPosX <  GetHorizontalPositionEnd()){
            GenerateTerrain(_lastGeneratedPosX,_terrainFirst);
            _lastGeneratedPosX += _terrainTemplateWidth;
      } 


     

    }
    
    private void Update() {
        while ( _lastGeneratedPosX < GetHorizontalPositionEnd()){
            //intinya selama posisi terakhir yg digenerate kurang dari offset akhir yg sudah disepakati dan selalu sejajar dengan karakter..
            GenerateTerrain(_lastGeneratedPosX,_terrainTemplateList);
            _lastGeneratedPosX += _terrainTemplateWidth;
        }

        while (_lastRemovedPosX + _terrainTemplateWidth < GetHorizontalPositionStart()){
            //selama garis HorStart menyentuh ekor(posisiAwal + panjang) dari terrain.
            _lastRemovedPosX += _terrainTemplateWidth;
            RemoveTerrain(_lastRemovedPosX);
        }


    }



    private void GenerateTerrain(float posX, List<ScTerrainTemplateController> terrain){
        GameObject newTerrain = Instantiate (terrain[Random.Range(0,terrain.Count)].gameObject,transform);
        newTerrain.transform.position = new Vector2(posX,0f);

        _spawnedTerrain.Add(newTerrain);

    }

    private void RemoveTerrain(float posx){
        GameObject terrainToRemove = null;

        foreach(GameObject item in _spawnedTerrain){
            if(item.transform.position.x < posx){
            terrainToRemove = item;        
            break;
            }
        }
        if( terrainToRemove !=null){
            _spawnedTerrain.Remove(terrainToRemove);
            Destroy(terrainToRemove);
        }

    }



    private float GetHorizontalPositionStart()
    {
        // float halfHeight = _camera.orthographicSize;
        // float halfWidth = _camera.aspect * halfHeight;
        // float horizontalMin = -halfWidth;
        // float horizontalMax =  halfWidth;

        //ini perbandingan mtk biasa, jangan lupa baro.

        return _camera.ViewportToWorldPoint(new Vector2(0f, 0f)).x + _areaStartOffset;
        //kurasa nanti pake width(bukan pixel) dari camera. biar camera bisa fleksibel
    }

    private float GetHorizontalPositionEnd()
    {
        
        return _camera.ViewportToWorldPoint(new Vector2(0f, 0f)).x + _areaEndOffset;
    }


    private void OnDrawGizmos()
    {
        Vector3 areaStartPosition = transform.position;
        Vector3 areaEndPosition = transform.position;

        areaStartPosition.x =  GetHorizontalPositionStart();
        areaEndPosition.x = GetHorizontalPositionEnd();

        Debug.DrawLine(areaStartPosition + Vector3.up * _debugLineHeight/2,areaStartPosition + Vector3.down * _debugLineHeight/2,Color.red);
        Debug.DrawLine(areaEndPosition + Vector3.up * _debugLineHeight/2,areaEndPosition + Vector3.down * _debugLineHeight/2,Color.red);
        



    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTerrainGeneratorPooling : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public int name;
        public ScTerrainTemplateController Terrain;
    }// ini gak terlalu penting hanya untuk mempermudah pemberian key/nama ke prefabnya...

    public List<Pool> _pools;

    private Dictionary<int, List<ScTerrainTemplateController>> poolDictionary;
    // dictionary--> nama, dan isinya.. isinya adalah bisa array/list/queue..
    //lebih baik pake queue lebih simple karena sistem antri.. tapi kali ini list saja...

    //beda dictionary dan list biasa adalah, dictionary tidak mengecek satu persatu sedangkan list dari indexnya..
    //gak ada pointnya pake dictionary kalau kalau ngecek satu persatu..



    [Header("Templates")]
    public List<ScTerrainTemplateController> _terrainFirst;
    public List<ScTerrainTemplateController> _terrainTemplateList;
    public float _terrainTemplateWidth = 1;



    [Header("Generator Area")]
    public Camera _camera;
    public float _areaStartOffset;
    public float _areaEndOffset;

    public const float _debugLineHeight = 10.0f;

    //Generator
    private List<GameObject> _spawnedTerrain;

    private float _lastGeneratedPosX;

    private float _lastRemovedPosX;


    void Start()
    {
        poolDictionary = new Dictionary<int, List<ScTerrainTemplateController>>();
        _spawnedTerrain = new List<GameObject>();
        float jmlObject = (_areaEndOffset - _areaStartOffset) / _terrainTemplateWidth + 2;
        //perhitungan abal2 di atass.. kalau kamera size di ubah bakalan kacau... tapi offsetnya juga kacau sih jadinya
        foreach (Pool pool in _pools)
        {


            List<ScTerrainTemplateController> listTerrain = new List<ScTerrainTemplateController>();
            for (int i = 0; i < jmlObject; i++)
            {//udah ku coba rupanya list didalam dictionary nya jumlahnya static.. pake list(gantikan dictionary) aja kalau projectile
                GameObject obj = Instantiate(pool.Terrain.gameObject);
                obj.transform.SetParent(gameObject.transform);
                obj.SetActive(false);
                // buat satu object di setiap poolD

                listTerrain.Add(obj.GetComponent<ScTerrainTemplateController>());
                //AAhhh udah terlanjur salah ini.. harusnya pake GameObject aja kelasnya.. terpaksa pake Getcomponent
            }
            poolDictionary.Add(pool.name, listTerrain);

        }

        _lastGeneratedPosX = GetHorizontalPositionStart();
        //Horizontal posisi start && Posisi End selalu sejajar dengan karakter
        _lastRemovedPosX = _lastGeneratedPosX - _terrainTemplateWidth;

        while (_lastGeneratedPosX < GetHorizontalPositionEnd())
        {
            GenerateFromPool(_lastGeneratedPosX,1);
            _lastGeneratedPosX += _terrainTemplateWidth;
        }

    }
    void Update()
    {
        while (_lastGeneratedPosX < GetHorizontalPositionEnd())
        {
            //intinya selama posisi terakhir yg digenerate kurang dari offset akhir yg sudah disepakati dan selalu sejajar dengan karakter..
            GenerateFromPool(_lastGeneratedPosX,poolDictionary.Count);
            _lastGeneratedPosX += _terrainTemplateWidth;
        }

        while (_lastRemovedPosX + _terrainTemplateWidth < GetHorizontalPositionStart())
        {
            //selama garis HorStart menyentuh ekor(posisiAwal + panjang) dari terrain.
            _lastRemovedPosX += _terrainTemplateWidth;
            RemoveTerrain(_lastRemovedPosX);
        }

    }
    private void GenerateFromPool(float posX, int maxRand)
    {

        int randomName = Random.Range(1, maxRand + 1);
        Debug.Log(randomName);
        foreach (ScTerrainTemplateController terrain in poolDictionary[randomName])
        {
            if (!terrain.gameObject.activeSelf)
            { //kalau gak aktif
                terrain.gameObject.SetActive(true);
                terrain.gameObject.transform.position = new Vector2(posX, 0f);
                _spawnedTerrain.Add(terrain.gameObject);
               
                break;//berhenti kalau uda ketemu yang gak aktif


            }

        }


    }
    private void RemoveTerrain(float posx)
    {
        GameObject terrainToRemove = null;

        foreach (GameObject item in _spawnedTerrain)
        {
            if (item.transform.position.x < posx)
            {
                terrainToRemove = item;
                break;
            }
        }
        if (terrainToRemove != null)
        {
            _spawnedTerrain.Remove(terrainToRemove);
            terrainToRemove.gameObject.SetActive(false);
        }

    }





    private float GetHorizontalPositionStart()
    {
        // float halfHeight = _camera.orthographicSize;
        // float halfWidth = _camera.aspect * halfHeight;
        // float horizontalMin = -halfWidth;
        // float horizontalMax =  halfWidth;

        //ini perbandingan mtk biasa, jangan lupa baro.

        return _camera.ViewportToWorldPoint(new Vector2(0f, 0f)).x + _areaStartOffset;
        //kurasa nanti pake width(bukan pixel) dari camera. biar camera bisa fleksibel
    }

    private float GetHorizontalPositionEnd()
    {

        return _camera.ViewportToWorldPoint(new Vector2(0f, 0f)).x + _areaEndOffset;
    }


    private void OnDrawGizmos()
    {
        Vector3 areaStartPosition = transform.position;
        Vector3 areaEndPosition = transform.position;

        areaStartPosition.x = GetHorizontalPositionStart();
        areaEndPosition.x = GetHorizontalPositionEnd();

        Debug.DrawLine(areaStartPosition + Vector3.up * _debugLineHeight / 2, areaStartPosition + Vector3.down * _debugLineHeight / 2, Color.red);
        Debug.DrawLine(areaEndPosition + Vector3.up * _debugLineHeight / 2, areaEndPosition + Vector3.down * _debugLineHeight / 2, Color.red);




    }



}
